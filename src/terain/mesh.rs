use crate::terain::world::CHUNK_SIZE;

use super::{chunks::Chunk, world::World};
use bevy::{
    math::IVec3,
    pbr::StandardMaterial,
    render::{
        mesh::{Indices, Mesh},
        render_resource::PrimitiveTopology,
    },
    transform::components::Transform,
};
#[derive(Clone)]
struct RawMesh {
    vertices: Vec<[f32; 3]>,
    indices: Vec<u32>,
}

impl RawMesh {
    fn new() -> Self {
        Self {
            vertices: Vec::new(),
            indices: Vec::new(),
        }
    }
    fn as_mesh(&self) -> Mesh {
        Mesh::new(PrimitiveTopology::TriangleList)
            .with_inserted_attribute(Mesh::ATTRIBUTE_POSITION, self.vertices.clone())
            .with_indices(Some(Indices::U32(self.indices.clone())))
    }
}

pub fn gen_chunk_bundle(chunk: &Chunk, world: &World) -> (Mesh, StandardMaterial, Transform) {
    let mesh = Direction::VALUES
        .iter()
        .flat_map(|dir| (0..CHUNK_SIZE).map(|i| gen_slice_mesh(chunk, i, dir, world)))
        .fold(RawMesh::new(), merge_raw_meshes)
        .as_mesh();

    (
        mesh,
        StandardMaterial::default(),
        Transform::from_translation(chunk.pos.as_vec3()),
    )
}

fn gen_slice_mesh(chunk: &Chunk, index: usize, direction: &Direction, world: &World) -> RawMesh {
    let drawable_faces = drawable_faces_of_a_slice(chunk, index, direction, world);
    let mut raw_meshes = Vec::new();
    for (x, line) in drawable_faces.iter().enumerate() {
        for (y, face) in line.iter().enumerate() {
            if !face {
                continue;
            }
            raw_meshes.push(gen_block_face(
                get_3d_pos((x, y), index, direction),
                direction,
            ));
        }
    }

    raw_meshes
        .into_iter()
        .fold(RawMesh::new(), merge_raw_meshes)
}

fn get_3d_pos(block_2d: (usize, usize), slice_index: usize, direction: &Direction) -> IVec3 {
    match direction {
        &Direction::NegX | &Direction::X => {
            IVec3::new(slice_index as i32, block_2d.0 as i32, block_2d.1 as i32)
        }
        &Direction::NegY | &Direction::Y => {
            IVec3::new(block_2d.0 as i32, slice_index as i32, block_2d.1 as i32)
        }
        &Direction::NegZ | &Direction::Z => {
            IVec3::new(block_2d.0 as i32, slice_index as i32, block_2d.1 as i32)
        }
    }
}

fn drawable_faces_of_a_slice(
    chunk: &Chunk,
    index: usize,
    direction: &Direction,
    world: &World,
) -> [[bool; CHUNK_SIZE]; CHUNK_SIZE] {
    let mut res = [[false; CHUNK_SIZE]; CHUNK_SIZE];

    for (x, line) in res.iter_mut().enumerate() {
        for (y, face) in line.iter_mut().enumerate() {
            *face = is_face_drawable(
                chunk,
                Chunk::ivec3_to_index(get_3d_pos((x, y), index, direction)),
                direction,
                world,
            );
        }
    }

    res
}
fn is_face_drawable(chunk: &Chunk, index: usize, direction: &Direction, world: &World) -> bool {
    let pos = Chunk::index_to_ivec3(index);
    if let Some(block) = world.get_block(&chunk.pos, pos) {
        return block.id != 0;
    }
    match direction {
        Direction::NegX => {
            if let Some(block) = world.get_block(&chunk.pos, pos - IVec3::X) {
                block.id == 0
            } else {
                false
            }
        }
        Direction::NegY => {
            if let Some(block) = world.get_block(&chunk.pos, pos - IVec3::Y) {
                block.id == 0
            } else {
                false
            }
        }
        Direction::NegZ => {
            if let Some(block) = world.get_block(&chunk.pos, pos - IVec3::Z) {
                block.id == 0
            } else {
                false
            }
        }
        Direction::X => {
            if let Some(block) = world.get_block(&chunk.pos, pos + IVec3::X) {
                block.id == 0
            } else {
                false
            }
        }
        Direction::Y => {
            if let Some(block) = world.get_block(&chunk.pos, pos + IVec3::Y) {
                block.id == 0
            } else {
                false
            }
        }
        Direction::Z => {
            if let Some(block) = world.get_block(&chunk.pos, pos + IVec3::Z) {
                block.id == 0
            } else {
                false
            }
        }
    }
}

fn gen_block_face(pos: IVec3, direction: &Direction) -> RawMesh {
    let mut raw_mesh = RawMesh::new();
    match direction {
        Direction::Y => {
            let vertices = vec![
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z + 0.5,
                ],
            ];
            let indices = vec![0, 3, 1, 1, 3, 2];

            raw_mesh.vertices = vertices;
            raw_mesh.indices = indices;
        }
        Direction::NegY => {
            let vertices = vec![
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z + 0.5,
                ],
            ];

            let indices = vec![0, 1, 3, 1, 2, 3];

            raw_mesh.vertices = vertices;
            raw_mesh.indices = indices;
        }
        Direction::X => {
            let vertices = vec![
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z - 0.5,
                ],
            ];

            let indices = vec![0, 3, 1, 1, 3, 2];

            raw_mesh.vertices = vertices;
            raw_mesh.indices = indices;
        }
        Direction::NegX => {
            let vertices = vec![
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z - 0.5,
                ],
            ];

            let indices = vec![0, 1, 3, 1, 2, 3];

            raw_mesh.vertices = vertices;
            raw_mesh.indices = indices;
        }
        Direction::Z => {
            let vertices = vec![
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z + 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z + 0.5,
                ],
            ];

            let indices = vec![0, 3, 1, 1, 3, 2];

            raw_mesh.vertices = vertices;
            raw_mesh.indices = indices;
        }
        Direction::NegZ => {
            let vertices = vec![
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x - 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y + 0.5,
                    pos.as_vec3().z - 0.5,
                ],
                [
                    pos.as_vec3().x + 0.5,
                    pos.as_vec3().y - 0.5,
                    pos.as_vec3().z - 0.5,
                ],
            ];

            let indices = vec![0, 1, 3, 1, 2, 3];

            raw_mesh.vertices = vertices;
            raw_mesh.indices = indices;
        }
    }

    raw_mesh
}
fn merge_raw_meshes(a: RawMesh, mut b: RawMesh) -> RawMesh {
    let mut new_raw_mesh = RawMesh::new();
    new_raw_mesh.vertices = a.vertices;
    new_raw_mesh.indices = a.indices;
    let offset = new_raw_mesh.vertices.len() as u32;

    for indice in b.indices {
        new_raw_mesh.indices.push(indice + offset);
    }
    new_raw_mesh.vertices.append(&mut b.vertices);

    new_raw_mesh
}
enum Direction {
    X,
    Y,
    Z,
    NegX,
    NegY,
    NegZ,
}
impl Direction {
    const VALUES: [Self; 6] = [
        Self::NegX,
        Self::NegY,
        Self::NegZ,
        Self::X,
        Self::Y,
        Self::Z,
    ];
}
