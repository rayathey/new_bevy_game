use super::chunks::{Block, Chunk};
use super::mesh::gen_chunk_bundle;
use crate::player_logic::player::Player;
use bevy::{prelude::*, utils::HashMap};
use num::integer::div_floor;
use rand::Rng;

const RENDED_DISTANCE: i32 = 5;
pub const CHUNK_SIZE: usize = 16;

#[derive(Resource)]
pub struct World {
    pub loaded_chunks: HashMap<IVec3, Chunk>,
    seed: u32,
}
impl World {
    pub fn new() -> Self {
        Self {
            loaded_chunks: HashMap::new(),
            seed: rand::thread_rng().gen(),
        }
    }
    fn load_chunk(&mut self, chunk_pos: IVec3) {
        self.loaded_chunks
            .insert(chunk_pos, Chunk::new(chunk_pos, self.seed));
    }
    fn chunk_is_loaded(&self, chunk_pos: &IVec3) -> bool {
        let result = self.loaded_chunks.get(chunk_pos);
        if result.is_some() {
            return true;
        }
        false
    }
    pub fn get_block(&self, chunk_pos: &IVec3, pos: IVec3) -> Option<Block> {
        if self.chunk_is_loaded(chunk_pos) {
            return self.loaded_chunks.get(chunk_pos).unwrap().get_block(pos);
        }
        None
    }
}

pub fn update(
    mut commands: Commands,
    mut world: ResMut<World>,
    player_query: Query<&Player>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let player_pos = player_query.single().get_pos().as_ivec3();
    for x in 0..RENDED_DISTANCE {
        for y in 0..RENDED_DISTANCE {
            for z in 0..RENDED_DISTANCE {
                let pos = IVec3 {
                    x: x - (RENDED_DISTANCE / 2) + div_floor(player_pos.x, CHUNK_SIZE as i32),
                    y: y - (RENDED_DISTANCE / 2) + div_floor(player_pos.y, CHUNK_SIZE as i32),
                    z: z - (RENDED_DISTANCE / 2) + div_floor(player_pos.z, CHUNK_SIZE as i32),
                };
                if world.chunk_is_loaded(&pos) {
                    continue;
                }
                world.load_chunk(pos);
                let chunk = world.loaded_chunks.get(&pos).unwrap();
                /* for (index, block) in chunk.blocks.iter().enumerate() {
                    let pos = IVec3::new(
                        (index % CHUNK_SIZE) as i32,
                        ((index / CHUNK_SIZE) % CHUNK_SIZE) as i32,
                        ((index / (CHUNK_SIZE * CHUNK_SIZE)) % CHUNK_SIZE) as i32,
                    );
                    if block.id == 0 {
                        continue;
                    }
                    for face in chunk.clone().block_faces(pos, &world).into_iter().flatten() {
                        let global_pos = pos + chunk.pos;
                        let tmp = 0.; //CHUNK_SIZE as f32 / 2.;
                        let transfomr = Transform::from_translation(
                            (global_pos.as_vec3() + face.as_vec3() * 0.5)
                                - Vec3::new(tmp, tmp, tmp),
                        )
                        .with_rotation(Quat::from_rotation_arc(Vec3::Y, face.as_vec3()));

                        commands.spawn((
                            PbrBundle {
                                mesh: meshes.add(Mesh::from(shape::Plane::from_size(1.))),
                                material: materials.add(
                                    Color::from((pos.as_vec3() / (CHUNK_SIZE as f32)).to_array())
                                        .into(),
                                ),
                                transform: transfomr,
                                ..default()
                            },
                            chunk.clone(),
                        ));
                    }
                } */
                let (new_mesh, new_material, new_transform) = gen_chunk_bundle(chunk, &world);
                commands.spawn((
                    PbrBundle {
                        mesh: meshes.add(new_mesh),
                        material: materials.add(new_material),
                        transform: new_transform,
                        ..default()
                    },
                    chunk.clone(),
                ));
            }
        }
    }
    world.loaded_chunks.retain(|chunk, _| {
        if ((player_pos.x) - (RENDED_DISTANCE / 2)..(player_pos.x) + (RENDED_DISTANCE / 2))
            .contains(&chunk.x)
            && ((player_pos.z) - (RENDED_DISTANCE / 2)..(player_pos.z) + (RENDED_DISTANCE / 2))
                .contains(&chunk.z)
            && ((player_pos.y) - (RENDED_DISTANCE / 2)..(player_pos.y) + (RENDED_DISTANCE / 2))
                .contains(&chunk.y)
        {
            return true;
        }
        false
    });
}
