use bevy::{math::ivec3, prelude::*};
use noise::{NoiseFn, Perlin};

use super::world::{World, CHUNK_SIZE};

#[derive(Clone, Component)]
pub struct Chunk {
    pub blocks: [Block; CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE],
    pub pos: IVec3,
}
impl Chunk {
    pub fn new(pos: IVec3, seed: u32) -> Self {
        let mut blocks = [Block { id: 1 }; CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];

        let perlin = Perlin::new(seed);
        let pos = pos * (CHUNK_SIZE as i32);

        for (i, block) in blocks.iter_mut().enumerate() {
            let tmp = Chunk::index_to_ivec3(i);
            let block_noise = (perlin.get([
                ((tmp.x as f64) + pos.x as f64) / (CHUNK_SIZE as f64),
                ((tmp.y as f64) + pos.y as f64) / (CHUNK_SIZE as f64),
                ((tmp.z as f64) + pos.z as f64) / (CHUNK_SIZE as f64),
            ]) + 1.)
                / 2.;
            if ((tmp.y as f64 + pos.y as f64) / CHUNK_SIZE as f64)
            // 0.5 
            > block_noise
            {
                *block = Block { id: 0 };
            }
        }

        Self { blocks, pos }
    }
    pub fn _block_faces(&self, pos: IVec3) -> [Option<IVec3>; 6] {
        let offsets = [
            ivec3(-1, 0, 0),
            ivec3(1, 0, 0),
            ivec3(0, -1, 0),
            ivec3(0, 1, 0),
            ivec3(0, 0, -1),
            ivec3(0, 0, 1),
        ];
        let mut res: [Option<IVec3>; 6] = [None; 6];
        for (i, offset) in offsets.into_iter().enumerate() {
            let neighbor = self.get_block(pos + offset);
            if neighbor.is_none() || neighbor.unwrap().id == 0 {
                res[i] = Some(offset);
            }
        }

        res
    }
    pub fn get_block(&self, pos: IVec3) -> Option<Block> {
        for i in pos.to_array() {
            if i <= 0 || i >= (CHUNK_SIZE - 1) as i32 {
                return None;
            }
        }

        self.blocks.get(Chunk::ivec3_to_index(pos)).copied()
    }
    /// this functions sole purpos is to convert the index of a chunks blocks to a relative position. Do not try to use it for global positions or an index that's max size isn't CHUNK_SIZE cubed.
    pub fn index_to_ivec3(index: usize) -> IVec3 {
        ivec3(
            (index % CHUNK_SIZE) as i32,
            ((index / CHUNK_SIZE) % CHUNK_SIZE) as i32,
            (((index / CHUNK_SIZE) / CHUNK_SIZE) % CHUNK_SIZE) as i32,
        )
    }
    /// this functions sole purpos is to convert a relative chunk position into a index, used to acces the blocks variable in a chunck. any negative numbers will be converted to 0. don't expect anything other than a usize in the range of 0 to CHUNK_SIZE cubed.
    pub fn ivec3_to_index(vec: IVec3) -> usize {
        vec.x as usize
            + (vec.y as usize * CHUNK_SIZE)
            + (vec.z as usize * (CHUNK_SIZE * CHUNK_SIZE))
    }
}

#[derive(Default, Component, Clone, Copy)]
pub struct Block {
    pub id: i8,
}

pub fn update_chunks(
    mut commands: Commands,
    world: Res<World>,
    drawn_chunks_query: Query<(Entity, &Chunk), With<Chunk>>,
) {
    for (drawn_chunk, chunk) in &drawn_chunks_query {
        if world.loaded_chunks.contains_key(&chunk.pos) {
            continue;
        }
        commands.entity(drawn_chunk).despawn();
    }
}
