use bevy::prelude::*;

pub mod chunks;
mod mesh;
pub mod world;
pub struct TerainPlugin;

impl Plugin for TerainPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(world::World::new());
        // app.add_systems(Startup, chunks::gen_block);
        app.add_systems(Update, world::update);
        app.add_systems(Update, chunks::update_chunks);
    }
}
