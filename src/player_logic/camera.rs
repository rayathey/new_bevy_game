use bevy::prelude::*;

use super::player::Player;

#[derive(Component)]
pub struct Cam;

pub fn setup(mut commands: Commands) {
    commands.spawn((
        Camera3dBundle { ..default() },
        FogSettings {
            color: Color::rgba(0.35, 0.48, 0.66, 1.0),
            directional_light_color: Color::rgba(1.0, 0.95, 0.85, 0.5),
            directional_light_exponent: 30.0,
            falloff: FogFalloff::from_visibility_colors(
                45.0, // distance in world units up to which objects retain visibility (>= 5% contrast)
                Color::rgb(0.35, 0.5, 0.66), // atmospheric extinction color (after light is lost due to absorption by atmospheric particles)
                Color::rgb(0.8, 0.844, 1.0), // atmospheric inscattering color (light gained due to scattering from the sun)
            ),
        },
        Cam,
    ));
}
pub fn update(player_query: Query<&Player>, mut camera_query: Query<&mut Transform, With<Cam>>) {
    let player = player_query.single();
    let mut cam = camera_query.single_mut();
    cam.translation = player.get_pos();
    cam.rotation = player.get_cam_rotation();
    let cam_move_val = cam.back() * player.get_cam_distance();
    cam.translation += cam_move_val;
}
