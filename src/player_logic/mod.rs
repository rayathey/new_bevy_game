use bevy::prelude::*;

mod camera;
pub mod player;
pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, player::player_update);
        app.add_systems(Update, player::mouse_camera_rotation);
        app.add_systems(Update, player::player_movement);
        app.add_systems(Startup, player::setup);

        app.add_systems(Startup, camera::setup);
        app.add_systems(Update, camera::update);
    }
}
