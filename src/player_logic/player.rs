use std::f32::consts::FRAC_PI_2;

use bevy::{
    input::mouse::MouseMotion,
    math::vec3,
    prelude::*,
    window::{CursorGrabMode, PrimaryWindow},
};

const SPEED: f32 = 12.;
const ROTATION_SPEED: f32 = 0.00125;

#[derive(Component)]
pub struct Player {
    cam_pitch: f32,
    cam_yaw: f32,
    cam_distance: f32,
    trans: Transform,
}

impl Player {
    fn new() -> Self {
        Self {
            cam_pitch: 0.,
            cam_yaw: 0.,
            cam_distance: 10.,
            trans: Transform::default(),
        }
    }
    pub fn get_pos(&self) -> Vec3 {
        self.trans.translation
    }
    pub fn get_cam_rotation(&self) -> Quat {
        Quat::from_euler(EulerRot::YXZ, self.cam_yaw, self.cam_pitch, 0.)
    }
    pub fn get_cam_distance(&self) -> f32 {
        self.cam_distance
    }
}

pub fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands.spawn((
        PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Cube { size: 1. })),
            material: materials.add(Color::PURPLE.into()),
            ..default()
        },
        Player::new(),
    ));
}

pub fn mouse_camera_rotation(
    mut player_query: Query<&mut Player>,
    mouse_buttons_input: Res<Input<MouseButton>>,
    mut q_windows: Query<&mut Window, With<PrimaryWindow>>,
    mut motion_event_reader: EventReader<MouseMotion>,
) {
    let mut primary_window = q_windows.single_mut();
    let mut player = player_query.single_mut();
    let window_size = Vec2::new(primary_window.width() / 2., primary_window.height() / 2.);
    if mouse_buttons_input.just_pressed(MouseButton::Middle) {
        primary_window.cursor.grab_mode = CursorGrabMode::Locked;
        primary_window.cursor.visible = false;
        primary_window.set_cursor_position(Some(window_size));
    }
    if mouse_buttons_input.pressed(MouseButton::Middle) {
        for event in motion_event_reader.read() {
            player.cam_yaw -= event.delta.x * ROTATION_SPEED;
            player.cam_pitch -= event.delta.y * ROTATION_SPEED;
            player.cam_pitch = player.cam_pitch.clamp(-FRAC_PI_2, FRAC_PI_2);
        }
        primary_window.set_cursor_position(Some(window_size));
    }
    if mouse_buttons_input.just_released(MouseButton::Middle) {
        primary_window.cursor.grab_mode = CursorGrabMode::Confined;
        primary_window.cursor.visible = true;
    }
}

pub fn player_update(
    player_query: Query<&Player>,
    mut player_model_transform: Query<&mut Transform, With<Player>>,
) {
    let player = player_query.single();
    let mut model_transform = player_model_transform.single_mut();
    *model_transform = player.trans;
}

pub fn player_movement(
    mut player_query: Query<&mut Player>,
    time: Res<Time>,
    keys: Res<Input<KeyCode>>,
) {
    let mut player = player_query.single_mut();
    let delta = time.delta().as_secs_f32();
    let offset = vec![
        KeyCode::A,
        KeyCode::D,
        KeyCode::W,
        KeyCode::S,
        KeyCode::Space,
        KeyCode::ControlLeft,
    ]
    .into_iter()
    .map(|code| to_offset(&keys, code))
    .fold(Vec3::ZERO, |a, b| a + b);
    if offset != Vec3::ZERO {
        // Rotate player
        player.trans.rotation = Quat::from_euler(EulerRot::YXZ, player.cam_yaw, 0., 0.);
    }
    let move_value = player.trans.rotation.mul_vec3(offset);
    player.trans.translation += SPEED * delta * move_value;
}

fn to_offset(keys: &Res<Input<KeyCode>>, code: KeyCode) -> Vec3 {
    if !keys.pressed(code) {
        return Vec3::ZERO;
    }
    match code {
        KeyCode::A => vec3(-1., 0., 0.),
        KeyCode::D => vec3(1., 0., 0.),
        KeyCode::W => vec3(0., 0., -1.),
        KeyCode::S => vec3(0., 0., 1.),
        KeyCode::Space => vec3(0., 1., 0.),
        KeyCode::ControlLeft => vec3(0., -1., 0.),
        _ => Vec3::ZERO,
    }
}
